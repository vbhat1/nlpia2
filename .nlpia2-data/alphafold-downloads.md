## Downloads

-   [Model organism proteomes](moz-extension://cdafa365-8115-4570-be9e-8855265a877b/download#proteomes-section)
-   [Global health proteomes](moz-extension://cdafa365-8115-4570-be9e-8855265a877b/download#global-health-section)
-   [Swiss-Prot](moz-extension://cdafa365-8115-4570-be9e-8855265a877b/download#swissprot-section)

AlphaFold DB currently provides predicted structures for the organisms listed below, as well as the majority of [Swiss-Prot.](https://www.expasy.org/resources/uniprotkb-swiss-prot)

You can download a prediction for an individual UniProt accession by visiting the corresponding structure page (example: [https://www.alphafold.ebi.ac.uk/entry/F4HVG8](moz-extension://cdafa365-8115-4570-be9e-8855265a877b/entry/F4HVG8)).

For downloading all predictions for a given species, use the download links below. Note that this option is only available on the desktop version of the site.

These uncompressed archive files (.tar) contain all the available compressed PDB and mmCIF files (.gz) for a reference proteome. In the case of proteins longer than 2700 amino acids (aa), AlphaFold provides 1400aa long, overlapping fragments. For example, Titin has predicted fragment structures named as Q8WZ42-F1 (residues 1–1400), Q8WZ42-F2 (residues 201–1600), etc. These fragments are currently only available in these proteome archive files, not on the website.

For downloading all predictions for all species, visit the FTP site: [ftp://ftp.ebi.ac.uk/pub/databases/alphafold](https://ftp.ebi.ac.uk/pub/databases/alphafold)

### Compressed prediction files for model organism proteomes:

Species

Common Name

Reference Proteome

Predicted Structures

Download

Arabidopsis thaliana

Arabidopsis

[UP000006548](https://www.uniprot.org/proteomes/UP000006548)

27,434

[Download (3,678 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000006548_3702_ARATH_v2.tar)

Caenorhabditis elegans

Nematode worm

[UP000001940](https://www.uniprot.org/proteomes/UP000001940)

19,694

[Download (2,626 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000001940_6239_CAEEL_v2.tar)

Candida albicans

C. albicans

[UP000000559](https://www.uniprot.org/proteomes/UP000000559)

5,974

[Download (974 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000559_237561_CANAL_v2.tar)

Danio rerio

Zebrafish

[UP000000437](https://www.uniprot.org/proteomes/UP000000437)

24,664

[Download (4,180 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000437_7955_DANRE_v2.tar)

Dictyostelium discoideum

Dictyostelium

[UP000002195](https://www.uniprot.org/proteomes/UP000002195)

12,622

[Download (2,171 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002195_44689_DICDI_v2.tar)

Drosophila melanogaster

Fruit fly

[UP000000803](https://www.uniprot.org/proteomes/UP000000803)

13,458

[Download (2,195 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000803_7227_DROME_v2.tar)

Escherichia coli

E. coli

[UP000000625](https://www.uniprot.org/proteomes/UP000000625)

4,363

[Download (453 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000625_83333_ECOLI_v2.tar)

Glycine max

Soybean

[UP000008827](https://www.uniprot.org/proteomes/UP000008827)

55,799

[Download (7,211 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000008827_3847_SOYBN_v2.tar)

Homo sapiens

Human

[UP000005640](https://www.uniprot.org/proteomes/UP000005640)

23,391

[Download (4,830 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000005640_9606_HUMAN_v2.tar)

Methanocaldococcus jannaschii

M. jannaschii

[UP000000805](https://www.uniprot.org/proteomes/UP000000805)

1,773

[Download (172 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000805_243232_METJA_v2.tar)

Mus musculus

Mouse

[UP000000589](https://www.uniprot.org/proteomes/UP000000589)

21,615

[Download (3,581 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000589_10090_MOUSE_v2.tar)

Oryza sativa

Asian rice

[UP000059680](https://www.uniprot.org/proteomes/UP000059680)

43,649

[Download (4,461 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000059680_39947_ORYSJ_v2.tar)

Rattus norvegicus

Rat

[UP000002494](https://www.uniprot.org/proteomes/UP000002494)

21,272

[Download (3,437 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002494_10116_RAT_v2.tar)

Saccharomyces cerevisiae

Budding yeast

[UP000002311](https://www.uniprot.org/proteomes/UP000002311)

6,040

[Download (969 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002311_559292_YEAST_v2.tar)

Schizosaccharomyces pombe

Fission yeast

[UP000002485](https://www.uniprot.org/proteomes/UP000002485)

5,128

[Download (783 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002485_284812_SCHPO_v2.tar)

Zea mays

Maize

[UP000007305](https://www.uniprot.org/proteomes/UP000007305)

39,299

[Download (5,064 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000007305_4577_MAIZE_v2.tar)

### Compressed prediction files for global health proteomes:

Species

Common Name

Reference Proteome

Predicted Structures

Download

Ajellomyces capsulatus

Ajellomyces capsulatus

[UP000001631](https://www.uniprot.org/proteomes/UP000001631)

9,199

[Download (1,351 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000001631_447093_AJECG_v2.tar)

Brugia malayi

Brugia malayi

[UP000006672](https://www.uniprot.org/proteomes/UP000006672)

8,743

[Download (1,274 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000006672_6279_BRUMA_v2.tar)

Campylobacter jejuni

C. jejuni

[UP000000799](https://www.uniprot.org/proteomes/UP000000799)

1,620

[Download (173 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000799_192222_CAMJE_v2.tar)

Cladophialophora carrionii

Cladophialophora carrionii

[UP000094526](https://www.uniprot.org/proteomes/UP000094526)

11,170

[Download (1,716 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000094526_86049_9EURO1_v2.tar)

Dracunculus medinensis

Dracunculus medinensis

[UP000274756](https://www.uniprot.org/proteomes/UP000274756)

10,834

[Download (1,351 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000274756_318479_DRAME_v2.tar)

Enterococcus faecium

Enterococcus faecium

[UP000325664](https://www.uniprot.org/proteomes/UP000325664)

2,823

[Download (285 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000325664_1352_ENTFC_v2.tar)

Fonsecaea pedrosoi

Fonsecaea pedrosoi

[UP000053029](https://www.uniprot.org/proteomes/UP000053029)

12,509

[Download (1,999 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000053029_1442368_9EURO2_v2.tar)

Haemophilus influenzae

H. influenzae

[UP000000579](https://www.uniprot.org/proteomes/UP000000579)

1,662

[Download (173 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000579_71421_HAEIN_v2.tar)

Helicobacter pylori

H. pylori

[UP000000429](https://www.uniprot.org/proteomes/UP000000429)

1,538

[Download (165 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000429_85962_HELPY_v2.tar)

Klebsiella pneumoniae

K. pneumoniae

[UP000007841](https://www.uniprot.org/proteomes/UP000007841)

5,727

[Download (554 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000007841_1125630_KLEPH_v2.tar)

Leishmania infantum

L. infantum

[UP000008153](https://www.uniprot.org/proteomes/UP000008153)

7,924

[Download (1,496 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000008153_5671_LEIIN_v2.tar)

Madurella mycetomatis

Madurella mycetomatis

[UP000078237](https://www.uniprot.org/proteomes/UP000078237)

9,561

[Download (1,525 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000078237_100816_9PEZI1_v2.tar)

Mycobacterium leprae

Mycobacterium leprae

[UP000000806](https://www.uniprot.org/proteomes/UP000000806)

1,602

[Download (175 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000806_272631_MYCLE_v2.tar)

Mycobacterium tuberculosis

M. tuberculosis

[UP000001584](https://www.uniprot.org/proteomes/UP000001584)

3,988

[Download (425 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000001584_83332_MYCTU_v2.tar)

Mycobacterium ulcerans

Mycobacterium ulcerans

[UP000020681](https://www.uniprot.org/proteomes/UP000020681)

9,033

[Download (575 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000020681_1299332_MYCUL_v2.tar)

Neisseria gonorrhoeae

N. gonorrhoeae

[UP000000535](https://www.uniprot.org/proteomes/UP000000535)

2,106

[Download (194 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000535_242231_NEIG1_v2.tar)

Nocardia brasiliensis

Nocardia brasiliensis

[UP000006304](https://www.uniprot.org/proteomes/UP000006304)

8,372

[Download (863 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000006304_1133849_9NOCA1_v2.tar)

Onchocerca volvulus

Onchocerca volvulus

[UP000024404](https://www.uniprot.org/proteomes/UP000024404)

12,047

[Download (1,607 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000024404_6282_ONCVO_v2.tar)

Paracoccidioides lutzii

Paracoccidioides lutzii

[UP000002059](https://www.uniprot.org/proteomes/UP000002059)

8,794

[Download (1,284 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002059_502779_PARBA_v2.tar)

Plasmodium falciparum

P. falciparum

[UP000001450](https://www.uniprot.org/proteomes/UP000001450)

5,187

[Download (1,142 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000001450_36329_PLAF7_v2.tar)

Pseudomonas aeruginosa

P. aeruginosa

[UP000002438](https://www.uniprot.org/proteomes/UP000002438)

5,556

[Download (608 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002438_208964_PSEAE_v2.tar)

Salmonella typhimurium

S. typhimurium

[UP000001014](https://www.uniprot.org/proteomes/UP000001014)

4,526

[Download (474 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000001014_99287_SALTY_v2.tar)

Schistosoma mansoni

Schistosoma mansoni

[UP000008854](https://www.uniprot.org/proteomes/UP000008854)

13,865

[Download (2,524 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000008854_6183_SCHMA_v2.tar)

Shigella dysenteriae

S. dysenteriae

[UP000002716](https://www.uniprot.org/proteomes/UP000002716)

3,893

[Download (370 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002716_300267_SHIDS_v2.tar)

Sporothrix schenckii

Sporothrix schenckii

[UP000018087](https://www.uniprot.org/proteomes/UP000018087)

8,652

[Download (1,507 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000018087_1391915_SPOS1_v2.tar)

Staphylococcus aureus

S. aureus

[UP000008816](https://www.uniprot.org/proteomes/UP000008816)

2,888

[Download (271 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000008816_93061_STAA8_v2.tar)

Streptococcus pneumoniae

S. pneumoniae

[UP000000586](https://www.uniprot.org/proteomes/UP000000586)

2,030

[Download (201 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000000586_171101_STRR6_v2.tar)

Strongyloides stercoralis

Strongyloides stercoralis

[UP000035681](https://www.uniprot.org/proteomes/UP000035681)

12,613

[Download (1,878 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000035681_6248_STRER_v2.tar)

Trichuris trichiura

Trichuris trichiura

[UP000030665](https://www.uniprot.org/proteomes/UP000030665)

9,564

[Download (1,350 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000030665_36087_TRITR_v2.tar)

Trypanosoma brucei

Trypanosoma brucei

[UP000008524](https://www.uniprot.org/proteomes/UP000008524)

8,491

[Download (1,334 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000008524_185431_TRYB2_v2.tar)

Trypanosoma cruzi

T. cruzi

[UP000002296](https://www.uniprot.org/proteomes/UP000002296)

19,036

[Download (2,934 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000002296_353153_TRYCC_v2.tar)

Wuchereria bancrofti

Wuchereria bancrofti

[UP000270924](https://www.uniprot.org/proteomes/UP000270924)

12,721

[Download (1,404 MB)](https://ftp.ebi.ac.uk/pub/databases/alphafold/latest/UP000270924_6293_WUCBA_v2.tar)

### Compressed prediction files for Swiss-Prot:

### Feedback

If you want to share feedback on an AlphaFold structure prediction please contact [alphafold@deepmind.com](mailto:alphafold@deepmind.com). If you have feedback on the website or experience any bugs please contact [afdbhelp@ebi.ac.uk](mailto:afdbhelp@ebi.ac.uk).

### Disclaimer

The AlphaFold Data and other information provided on this site is for theoretical modelling only, caution should be exercised in its use. It is provided 'as-is' without any warranty of any kind, whether expressed or implied. For clarity, no warranty is given that use of the information shall not infringe the rights of any third party. The information is not intended to be a substitute for professional medical advice, diagnosis, or treatment, and does not constitute medical or other professional advice.

Use of the AlphaFold Protein Structure Database is subject to EMBL-EBI [Terms of Use](https://www.ebi.ac.uk/about/terms-of-use/).